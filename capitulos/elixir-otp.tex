\chapter{Programação Concorrente utilizando Elixir e OTP}

De acordo com \citeonline{logan2010erlang}, a linguagem Erlang teve seu início na década de 80 pelas mãos de Joe Armstrong, como um projeto interno da empresa Ericsson, tendo como motivação principal a falta de uma linguagem que pudesse desempenhar concorrência de forma simples e prática. No ano de 1995 ocorreu o colapso de um grande projeto baseado na linguagem C++, e a Ericsson decidiu utilizar a linguagem Erlang para reescrevê-lo, criando um time exclusivo de suporte à linguagem para auxiliar os desenvolvedores no processo. O time de suporte então criou um \textit{framework} para normalizar as mais de um milhão de linhas de código geradas no projeto. Este \textit{framework} ficou conhecido como \textit{Open Telecom Platform}, ou OTP.

O \textit{framework} OTP define uma série de comportamentos que podem ser apresentados por processos em um sistema concorrente. Entre eles podemos destacar:

\begin{itemize}
\item \lstinline|gen_server| - representa um servidor genérico - um processo que recebe mensagens de forma síncrona e assíncrona, enquanto mantém um estado interno que somente pode ser acessado e alterado pelo próprio processo.

\item \lstinline|supervisor| - representa um supervisor de processos - um processo que mantém outros processos em execução, reiniciando-os em caso de erros e de acordo com sua estratégia de supervisão.

\item \lstinline|application| - representa uma aplicação - um processo que, quando iniciado, irá inicializar também toda a árvore de supervisão de uma aplicação.
\end{itemize}

Estes comportamentos podem ser comparados às interfaces da programação orientada a objetos: são uma série de funções que devem ser implementadas em módulos que os deseje implementar.

\begin{figure}[hbt]
	\caption{\label{erlang_module_exemplo}Exemplo da definição de um módulo Erlang}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}, language=Erlang]
-module(my_list).

-export([sum/1, change_list/1]).

sum([]) -> 0;
sum([Head|Tail]) ->
	Head + sum(Tail).
	
change_list(List) ->
	List1 = lists:map(fun(N) -> N * 2 end, List),
	List2 = lists:filter(fun(N) -> N > 5 end, List1),
	sum(List2).
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Porém, apesar de todos os testes pelos quais a plataforma Erlang/OTP já passou e a confiabilidade demonstrada em todos eles, um dos pontos que ainda afasta desenvolvedores é a sua sintaxe, considerada confusa devido a sua inspiração na linguagem Prolog. Um exemplo de código pode ser encontrado na \autoref{erlang_module_exemplo}.

Com esta problemática em mente, um brasileiro chamado José Valim decidiu criar a linguagem de programação Elixir, que pode ser executada na mesma máquina virtual da linguagem Erlang, compartilhando assim bibliotecas e todos os aperfeiçoamentos desenvolvidos nos anos de existência da plataforma Erlang/OTP, ao mesmo tempo que trazendo uma sintaxe renovada, altamente inspirada na linguagem de programação Ruby. O código do mesmo módulo da \autoref{erlang_module_exemplo} escrito em Elixir pode ser visualizado na \autoref{elixir_module_exemplo}.

\begin{figure}[hbt]
	\caption{\label{elixir_module_exemplo}Exemplo da definição de um módulo Elixir}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule MyList do
  def sum([]), do: 0
  def sum([head|tail]) do
    head + sum(tail)
  end

  def change_list(list) do
    list
    |> Enum.map(&(&1 * 2))
    |> Enum.filter(&(&1 > 5))
    |> sum
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Os quatro comportamentos do OTP citados anteriormente são implementados, respectivamente, pelos seguintes módulos da biblioteca padrão da linguagem Elixir:

\begin{itemize}
\item \lstinline|GenServer|
\item \lstinline|Supervisor|
\item \lstinline|Application|
\end{itemize}

No decorrer deste capítulo cada um destes comportamentos será abordado, exemplificando a utilização e exemplos de uso de cada um.

\section{GenServer}

É o mais comum dos tipos de processo e, devido a sua alta flexibilidade, é utilizado como base para a construção dos demais tipos. Este comportamento requer a definição das seguintes funções:

\begin{itemize}
\item \lstinline|init| - chamada quando o processo é iniciado - recebe como argumento os parâmetros de inicialização do processo e deve retornar se ele foi iniciado corretamente ou não, retornando também o estado inicial do processo no primeiro caso

\item \lstinline|handle_cast| - chamada quando uma mensagem assíncrona é recebida - recebe como argumentos a mensagem e o estado atual do processo, e deve retornar o quê o processo deve fazer a seguir (aguardar uma nova mensagem ou se desligar, por exemplo) juntamente com o novo estado do processo.

\item \lstinline|handle_call| - chamada quando uma mensagem síncrona é recebida - recebe como argumentos a mensagem, uma referência ao processo remetente e o estado atual do processo, e deve retornar o quê o processo deve fazer a seguir juntamente com o novo estado do processo.

\item \lstinline|handle_info| - chamada quando uma mensagem comum é recebida - recebe como argumentos a mensagem e o estado atual do processo, e deve retornar o quê o processo deve fazer a seguir juntamente com o novo estado do processo.

\item \lstinline|code_change| - chamada quando o código fonte da implementação é alterado - recebe como argumento o número da versão anterior, o estado atual do processo e algumas informações extras necessárias para a alteração, e deve retornar o novo estado do processo alterado para a nova versão.

\item \lstinline|terminate| - chamada quando o processo é encerrado, seja por um erro ou outra situação qualquer - recebe como argumento o motivo do encerramento e o estado atual do processo.
\end{itemize}

A implementação de um processo representando uma pilha pode ser visualizada na \autoref{elixir_gen_server_exemplo}.

\begin{figure}[hbt]
	\caption{\label{elixir_gen_server_exemplo}Exemplo da implementação de um processo do tipo \lstinline|gen_server| em Elixir}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule Stack do
  @behaviour :gen_server

  def init(initial_stack) do
    {:ok, initial_stack}
  end

  def handle_call(:pop, _from, []), do: {:reply, nil, []}
  def handle_call(:pop, _from, [head | tail]) do
    {:reply, head, tail}
  end

  def handle_cast({:push, value}, stack) do
    {:noreply, [value | stack]}
  end

  def handle_info(_msg, stack) do
    {:noreply, stack}
  end

  def code_change(_old_version, stack, _extra) do
    {:ok, stack}
  end

  def terminate(_reason, _stack) do
    :ok
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Logo na segunda linha, o trecho "\lstinline|@behaviour :gen_server|" indica que este módulo está implementando o comportamento \lstinline|gen_server|, de modo que caso alguma das funções não seja encontrada, o compilador irá emitir um alerta.

A implementação da função \lstinline|init| indica que o processo poderá receber em sua inicialização uma lista que se tornará o estado inicial da pilha.

A função \lstinline|handle_call| é implementada duas vezes, ambas utilizando \textit{pattern match} para se certificar que a mensagem recebida é o \textit{atom} \lstinline|:pop| e ignorando a referência ao processo remetente. A primeira implementação utiliza \textit{pattern match} para se certificar que o estado atual é uma lista vazia e retornar um valor nulo. Já a segunda implementação utiliza \textit{pattern match} para desconstruir o estado atual, que deve ser uma lista, em seu primeiro elemento e uma lista contendo o restante, retornando o primeiro elemento e utilizando o restante como novo estado atual.

Para que a inserção de elementos na pilha ocorra de forma assíncrona, ele é implementado na função \lstinline|handle_cast|, que utiliza \textit{pattern match} para assegurar que a mensagem recebida seja uma tupla contendo o \textit{atom} \lstinline|:push| e um valor qualquer, e cria uma nova lista, utilizando o valor contido na mensagem como primeiro elemento e o estado atual como o restante, retornando-a como novo estado do processo.

As funções \lstinline|handle_info|, \lstinline|code_change| e \lstinline|terminate| são implementadas apenas devido a obrigatoriedade exigida pelo comportamento \lstinline|gen_server|, e suas implementações apenas fazem com que o processo, respectivamente, ignore qualquer qualquer mensagem recebida, utilize o mesmo estado em trocas de versão e não faça nada quando encerrado.

Esta obrigatoriedade pode ser eliminada utilizando a macro\footnote{funções presentes na linguagem Elixir que são executadas em tempo de compilação com o objetivo de modificar o código gerado} "\lstinline|use GenServer|", que traz implementações padrão para todas as funções do comportamento. Desta forma, o código apresentado na \autoref{elixir_gen_server_use_exemplo} apresenta a mesma funcionalidade do código apresentado na \autoref{elixir_gen_server_exemplo}.

\begin{figure}[hbt]
	\caption{\label{elixir_gen_server_use_exemplo}Exemplo da implementação de um processo do tipo \lstinline|gen_server| em Elixir utilizando facilidades da linguagem}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule Stack do
  use GenServer

  def handle_call(:pop, _from, []), do: {:reply, nil, []}
  def handle_call(:pop, _from, [head | tail]) do
    {:reply, head, tail}
  end

  def handle_cast({:push, value}, stack) do
    {:noreply, [value | stack]}
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Para a comunicação com processos que implementam este comportamento, a biblioteca padrão possui diversas funções. Entre elas podemos destacar:

\begin{itemize}
\item \lstinline|GenServer.start_link| - utilizada para inicializar um processo - recebe como argumentos o nome do módulo que implementa o comportamento, seus parâmetros de inicialização e, como argumento opcional, opções do \textit{framework} (nome para registro, prioridade no escalonador da máquina virtual e tipo de depuração são exemplos).

\item \lstinline|GenServer.cast| - utilizada para enviar uma mensagem assíncrona - recebe como argumentos um identificador do processo, seja ele um id de processo ou um nome registrado, e a mensagem em si, retornando o \textit{atom} \lstinline|:ok|.

\item \lstinline|GenServer.call| - utilizada para enviar uma mensagem síncrona - recebe como argumentos um identificador do processo, seja ele um id de processo ou um nome registrado, e a mensagem em si, retornando o valor retornado pelo processo.

\item \lstinline|GenServer.stop| - utilizada para encerrar um processo - recebe como argumentos um identificador do processo, seja ele um id de processo ou um nome registrado, um motivo para o encerramento e quanto tempo deve se aguardar para que o processo se encerre antes de tentar encerrá-lo de forma bruta.
\end{itemize}

A utilização destas funções em conjunto com o processo de pilha definido anteriormente pode ser visualizada na \autoref{elixir_gen_server_usage}.

\begin{figure}[hbt]
	\caption{\label{elixir_gen_server_usage}Exemplo da utilização do processo servidor definido na \autoref{elixir_gen_server_use_exemplo}}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}, numbers=none, framexleftmargin=0em, xleftmargin=1.5em]
{:ok, pid} = GenServer.start_link(Stack, [1, 2])
GenServer.cast(pid, {:push, 3}) # >> :ok
GenServer.call(pid, :pop) # >> 3
GenServer.call(pid, :pop) # >> 1
GenServer.call(pid, :pop) # >> 2
GenServer.call(pid, :pop) # >> nil
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Para tornar mais simples a reutilização dos processos em toda a aplicação, recomenda-se a criação de funções de interface no módulo que implementa o comportamento, fazendo com que detalhes, como a estrutura de mensagens e opções do \textit{framework}, não precisem ser conhecidos por todos que se comunicarem com o processo. Uma implementação do processo de pilha seguindo esta recomendação pode ser conferido na \autoref{elixir_gen_server_interface_exemplo}, e sua utilização, agora consideravelmente mais simples pode ser visualizada na \autoref{elixir_gen_server_interface_usage}.

\begin{figure}[hbt]
	\caption{\label{elixir_gen_server_interface_exemplo}Exemplo da implementação de um processo do tipo \lstinline|gen_server| em Elixir criando uma interface de comunicação}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule Stack do
  use GenServer

  # Public API
  def start_link, do: start_link([])
  def start_link(initial_stack) do
    GenServer.start_link(__MODULE__, initial_stack)
  end

  def pop(pid) do
    GenServer.call(pid, :pop)
  end

  def push(pid, value) do
    GenServer.cast(pid, {:push, value})
  end

  # GenServer callbacks
  def handle_call(:pop, _from, []), do: {:reply, nil, []}
  def handle_call(:pop, _from, [head | tail]) do
    {:reply, head, tail}
  end

  def handle_cast({:push, value}, stack) do
    {:noreply, [value | stack]}
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

\begin{figure}[hbt]
	\caption{\label{elixir_gen_server_interface_usage}Exemplo da utilização do processo servidor definido na \autoref{elixir_gen_server_interface_exemplo}}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}, numbers=none, framexleftmargin=0em, xleftmargin=1.5em]
{:ok, pid} = Stack.start_link([1, 2])
Stack.push(pid, 3) # >> :ok
Stack.pop(pid) # >> 3
Stack.pop(pid) # >> 1
Stack.pop(pid) # >> 2
Stack.pop(pid) # >> nil
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Caso a comunicação com o processo sem que se possua seu id se faça necessária, é possível registrá-lo informando um \textit{atom} na opção \lstinline|:name| que pode ser passada à função \lstinline|GenServer.start_link|. Por exemplo, considerando que só existirá uma instância do processo de pilha que vem sendo utilizado como exemplo, registrá-lo e utilizar seu nome na comunicação é uma alternativa mais atraente do que passar seu id de processo por toda a aplicação. A definição deste processo utilizando o registro de nomes e um exemplo de sua utilização, ainda mais simplificado que os anteriores, podem ser vistos, respectivamente na \autoref{elixir_gen_server_registro_exemplo} e na \autoref{elixir_gen_server_registro_usage}.

Uma das grandes vantagens de se utilizar processos registrados se mostra quando estes são utilizados em árvores de supervisão. Se a única referência disponível para um processo é seu id, caso um erro ocorra e o mesmo seja reiniciado, a referência perde a validade (visto que o id de processo foi alterado durante a reinicialização). Ao utilizar um processo registrado, caso o mesmo seja reiniciado, o nome não será alterado, tornando a sim desnecessária a preocupação em manter as referências ao processo em execução sempre atualizadas.

\begin{figure}[h]
	\caption{\label{elixir_gen_server_registro_exemplo}Exemplo da implementação de um processo do tipo \lstinline|gen_server| em Elixir utilizando registro}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule Stack do
  use GenServer

  # Public API
  def start_link, do: start_link([])
  def start_link(initial_stack) do
    GenServer.start_link(__MODULE__, initial_stack, name: :stack)
  end

  def pop do
    GenServer.call(:stack, :pop)
  end

  def push(value) do
    GenServer.cast(:stack, {:push, value})
  end

  # GenServer callbacks
  def handle_call(:pop, _from, []), do: {:reply, nil, []}
  def handle_call(:pop, _from, [head | tail]) do
    {:reply, head, tail}
  end

  def handle_cast({:push, value}, stack) do
    {:noreply, [value | stack]}
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

\begin{figure}[h]
	\caption{\label{elixir_gen_server_registro_usage}Exemplo da utilização do processo servidor definido na \autoref{elixir_gen_server_registro_exemplo}}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}, numbers=none, framexleftmargin=0em, xleftmargin=1.5em]
Stack.start_link([1, 2])
Stack.push(3) # >> :ok
Stack.pop # >> 3
Stack.pop # >> 1
Stack.pop # >> 2
Stack.pop # >> nil
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Porém um fator que merece atenção ao utilizar processos registrados é que os mesmos são únicos em todas as aplicações que estão em execução em uma instância da máquina virtual. Desta forma, para evitar conflitos entre diferentes aplicações, geralmente se utiliza o próprio nome do módulo como nome para registro.

\section{Supervisor}

Supervisores são elementos fundamentais em aplicações que buscam alta disponibilidade, pois mantém a aplicação em execução, reiniciando parte dela ou até mesmo toda aplicação em caso de erros. 

Este comportamento requer a definição de somente uma função: \lstinline|init|, que recebe os parâmetros de inicialização do supervisor e deve retornar a especificação da estrutura de supervisão. A especificação de uma estrutura de supervisão deve conter uma lista de especificações de processos a serem supervisionados e as configurações do supervisor em si.

As especificações de processos podem referenciar tanto processos comuns quanto outros supervisores. Elas contém as informações necessárias para iniciar - e reiniciar quando preciso - cada um dos processos filhos de um supervisor. Entre as informações encontradas nestas especificações podemos destacar:

\begin{itemize}
\item \textbf{Módulo, função e argumentos} - A definição da função que deverá ser executada para iniciar o processo e uma lista contendo os argumentos a serem passados. Devido a convenções da linguagem a função padrão é \lstinline|start_link|, porém o módulo e a lista de argumentos devem ser informados de forma obrigatória.

\item \textbf{Id} - Identificação única da especificação dentro das especificações do supervisor. Por padrão o valor do id é o nome do módulo de implementação, portanto caso exista mais de uma especificação utilizando o mesmo módulo um id deve ser informado.

\item \textbf{Método de reinício} - Determina como o processo deverá ser reiniciado. Os valores para esta configuração podem ser \lstinline|:permanent|, o processo sempre é reiniciado, \lstinline|:transient|, o processo somente é reiniciado se ele encerrar com um erro, e \lstinline|:temporary|, o processo nunca é reiniciado. O valor padrão para esta configuração é \lstinline|:permanent|.

\item \textbf{Método de encerramento} - Determina como o processo deverá ser encerrado, quando necessário. Os valores para esta configuração podem ser \lstinline|:brutal_kill|, o processo é encerrado imediatamente (caso seja um \lstinline|gen_server|, a função \lstinline|terminate| não é chamada), \lstinline|:infinity|, o supervisor aguarda indefinidamente enquanto o processo se encerra, e qualquer valor numérico, o supervisor aguarda a quantia especificada em milissegundos enquanto o processo se encerra, encerrando-o de forma bruta caso o tempo se esgote.
\end{itemize}

A linguagem de programação Elixir provê duas funções auxiliares para a criação destas especificações: \lstinline|worker| e \lstinline|supervisor|. Ambas esperam como argumentos o nome do módulo, uma lista de argumentos que serão passados ao inicializar o processo, e como argumento opcional as demais informações para a composição da especificação. A única diferença entre estas funções auxiliares é o fato de que a primeira deve ser utilizadas para processos em geral enquanto a última deve ser utilizada para outros supervisores.

Já as configurações do supervisor indicam como ele irá gerenciar seus processos filhos. As configurações suportadas são:

\begin{itemize}
\item \textbf{Máximo de reinícios e máximo de segundos} - Estas duas configurações, em conjunto, definem que se ocorrer um determinado número de reinícios em um determinado número de segundos, o supervisor deve ser encerrado. Esta configuração é muito útil em casos onde existe um erro de lógica em algum dos processos durante o desenvolvimento, e o mesmo seria reiniciado infinitas vezes até que o desenvolvedor pare a aplicação. Os valores padrão para esta configuração são 3 reinícios em 5 segundos.

\item \textbf{Estratégia de supervisão} - Esta configuração especifica como os processos deverão ser supervisionados. As estratégias de supervisão suportadas são quatro: \lstinline|:one_for_one|, cada processo morto é reiniciado, \lstinline|:one_for_all|, sempre que um processo for morto todos são reiniciados, \lstinline|:rest_for_one|, quando um processo é morto, ele e todos os processos iniciados após ele são reiniciados, e \lstinline|:simple_one_for_one|, similar a \lstinline|:one_for_one|, porém requer que exista apenas uma especificação de processo e os processos são iniciados de forma dinâmica. Esta configuração não possui um valor padrão e deve ser informada de forma obrigatória.
\end{itemize}

Para a criação da especificação da estrutura de supervisão, a linguagem provê a função auxiliar \lstinline|supervise|. Ela espera dois argumentos: uma lista, contendo a especificação dos processos, e as configurações do supervisor. 

Um supervisor para o processo de pilha criado anteriormente pode ser definido conforme exemplificado na \autoref{elixir_supervisor_exemplo}.

\begin{figure}[h]
	\caption{\label{elixir_supervisor_exemplo}Exemplo da implementação de um processo do tipo \lstinline|supervisor| em Elixir}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule StackSupervisor do
  use Supervisor
  
  def init(initial_stack) do
    children = [
      worker(Stack, [initial_stack])
    ]
     
    supervise children, strategy: :one_for_one
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Na segunda linha do código, a chamada "\lstinline|use Supervisor|" \ indica que este módulo irá implementar um processo do tipo supervisor e importa algumas funções úteis. 

A definição da função \lstinline|init| recebe o parâmetro de inicialização mas o descarta, constrói a lista de especificações de processos com o auxílio da função auxiliar \lstinline|worker| e passa a lista para a função auxiliar \lstinline|supervise|.

De acordo com esta implementação, ao inicializar o supervisor a função \lstinline|start_link| do módulo \lstinline|Stack| será chamada passando como argumento qualquer valor que tenha sido informado como parâmetro de inicialização para o supervisor. O processo inicializado com esta chamada será então supervisionado utilizando a estratégia de supervisão \lstinline|:one_for_one|.

Para a comunicação com supervisores, existem algumas funções na biblioteca padrão. Entre elas pode-se destacar:

\begin{itemize}
\item \lstinline|Supervisor.start_link| - utilizada para inicializar um supervisor - recebe como argumentos o nome do módulo que implementa o supervisor, seus parâmetros de inicialização e, como argumento opcional, opções do \textit{framework}. Geralmente apenas o nome para registro é utilizado como opção, mas as demais configurações do supervisor citadas anteriormente também podem ser utilizadas.

\item \lstinline|Supervisor.which_children| - utilizada para verificar quais processos estão sendo supervisionados - recebe como argumento uma referência ao supervisor e retorna uma lista com as informações dos processos supervisionados.

\item \lstinline|Supervisor.start_child| - utilizada para adicionar processo filhos dinamicamente ao supervisor - recebe como argumentos uma referência ao supervisor e uma especificação de processo, retornando uma referência ao processo iniciado ou um motivo caso o mesmo não possa ser inicializado.

\item \lstinline|Supervisor.delete_child| - utilizada para remover um processo filho da supervisão - recebe como argumentos uma referência ao supervisor e o id da especificação, retorna o \textit{atom} \lstinline|:ok| caso a especificação seja removida com sucesso.

\item \lstinline|Supervisor.stop| - utilizada para encerrar um supervisor - recebe como argumentos uma referência ao supervisor, um motivo para o encerramento e quanto tempo deve se aguardar para que o supervisor e seus processos se encerrem antes de tentar encerrá-los de forma bruta.
\end{itemize}

Utilizando estas funções, é possível iniciar o supervisor definido anteriormente conforme exemplificado pela \autoref{elixir_supervisor_usage}.

\begin{figure}[h]
	\caption{\label{elixir_supervisor_usage}Exemplo da utilização do supervisor definido na \autoref{elixir_supervisor_exemplo}}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}, numbers=none, framexleftmargin=0em, xleftmargin=1.5em]
Supervisor.start_link(StackSupervisor, [1, 2])
Stack.pop # >> 1
Stack.push(5) # >> :ok
Stack.pop # >> 5
Stack.pop # >> 2
Stack.pop # >> nil
GenServer.stop(:stack, :exit) # Processo de pilha finalizado
Stack.pop # >> 1
Stack.pop # >> 2
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

De forma semelhante à implementação de processos \lstinline|gen_server|, é considerada uma boa prática definir funções de interface no módulo implementador para ocultar detalhes da comunicação. Seguindo esta recomendação, a implementação deste supervisor ficaria como exibido na \autoref{elixir_supervisor_interface_exemplo}.

\begin{figure}[h]
	\caption{\label{elixir_supervisor_interface_exemplo}Exemplo da implementação de um processo do tipo \lstinline|supervisor| em Elixir utilizando uma interface de comunicação}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule StackSupervisor do
  use Supervisor
  
  def start_link(initial_stack) do
    Supervisor.start_link(StackSupervisor, initial_stack)
  end
  
  def init(initial_stack) do
    children = [
      worker(Stack, [initial_stack])
    ]
     
    supervise children, strategy: :one_for_one
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Supervisores também podem ser utilizados sem a necessidade de um módulo que os implemente, passando a lista de especificações de processos e as sua configuração diretamente à função \lstinline|Supervisor.start_link|, porém sua implementação em módulos possibilita uma definição mais clara da estrutura de supervisão da aplicação.

Por definição, supervisores devem se manter simples. Manter uma lógica extremamente simples em um supervisor faz com que a possibilidade de erros no mesmo seja minúscula. É por esta razão que eles não possuem nenhuma forma simples para se comunicar com outros processos a não ser pelas funções da biblioteca padrão da linguagem. 

Esta filosofia de desenvolvimento, combinada com a habilidade de se combinar quantos supervisores forem necessários em complexas estruturas de supervisão, são a base da alta capacidade deste \textit{framework} de criar aplicações altamente disponíveis. Desta forma, mesmo que um erro seja propagado de um processo ao seu supervisor, este supervisor também é supervisionado, fazendo com que o erro tenha que ser propagado por mais e mais supervisores até que a aplicação pare por completo, e, como os supervisores não possuem nenhuma lógica complexa, a probabilidade de um erro ocorrer nos mesmos e extremamente baixa.

\section{Application}

Cada aplicação possui apenas um processo deste tipo, e ele é a porta de entrada para a aplicação. Para a utilização deste processo é necessário definir duas funções no módulo de implementação: \lstinline|start| e \lstinline|stop|. 

A primeira recebe como argumentos o tipo de início da aplicação e os parâmetros de inicialização informados na configuração da aplicação. O valor do tipo de início geralmente é o \textit{atom} \lstinline|:normal|, porém este argumento pode receber outros valores em aplicações sendo executadas de forma distribuída. A função deve obrigatoriamente retornar o id de processo do supervisor principal da aplicação, e pode retornar juntamente um valor qualquer, que será utilizado como o estado da aplicação.

Já a função \lstinline|stop| pode ser omitida, devido à mesma possuir uma implementação padrão na linguagem Elixir. Ela é chamada quando a aplicação é encerrada e recebe com argumento o estado da aplicação. Sua utilização é basicamente para realizar qualquer tipo de limpeza necessário após a execução da aplicação.

Uma aplicação para utilizar os processos de pilha definidos anteriormente neste capitulo poderia ser inicializada com o código disposto na \autoref{elixir_application_exemplo}.

\begin{figure}[h]
	\caption{\label{elixir_application_exemplo}Exemplo da implementação de um processo do tipo \lstinline|application| em Elixir}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule StackApp do
  use Application
  
  def start(_type, _args) do
    StackSupervisor.start_link([])
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Diferentemente dos demais tipos de processo estudados, um módulo que implementa uma aplicação não é chamado diretamente pelo programador, e sim configurado como módulo representante da aplicação e é utilizado pelas bibliotecas internas da linguagem Erlang para iniciá-la. Esta configuração é realizada por meio do arquivo \lstinline|mix.exs|.

O arquivo \lstinline|mix.exs| deve conter algumas informações sobre o projeto, dentre as quais podemos destacar o nome do projeto e sua versão atual. Caso o projeto seja uma aplicação OTP, ele também deve definir quais aplicações precisam ser iniciadas antes e qual módulo representa a aplicação, juntamente com seus argumentos de inicialização. 

As informações do projeto devem ser retornadas pela função \lstinline|project|, enquanto as informações da aplicação OTP deve ser retornadas pela função \lstinline|application|. Um exemplo deste arquivo pode ser visualizado na \autoref{elixir_mix_exemplo}.

\begin{figure}[h]
	\caption{\label{elixir_mix_exemplo}Exemplo da implementação de um processo do tipo \lstinline|application| em Elixir}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}]
defmodule StackApp.MixFile do
  use Mix.Project
  
  def project do
    [app: :stack,
     version: "1.0.0"]
  end
  
  def application do
    [applications: [:logger],
     mod: {StackApp, []}]
  end
end
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Com base neste arquivo, a aplicação OTP \lstinline|:stack|, em sua versão \lstinline|1.0.0| será iniciada através da função \lstinline|start| do módulo \lstinline|StackApp|, que será chamada passando uma lista vazia como argumento, mas antes disso a aplicação \lstinline|:logger| será iniciada.

Para iniciar esta aplicação, basta em qualquer instância da máquina virtual Erlang que possua o código desta aplicação carregado executar o comando exibido na \autoref{elixir_application_usage}.

\begin{figure}[h]
	\caption{\label{elixir_application_usage}Exemplo da inicialização da aplicação definida na \autoref{elixir_application_exemplo} e configurada na \autoref{elixir_mix_exemplo}}
	\begin{center}
		\begin{lstlisting}[basicstyle={\ttfamily\tiny}, numbers=none, framexleftmargin=0em, xleftmargin=1.5em]
Application.ensure_all_started(:stack)
		\end{lstlisting}
	\end{center}
	\figurecite{Fonte: Próprio autor}
\end{figure}

Aplicações podem ser escritas tanto em Elixir quanto em Erlang, sendo possível até mesmo misturar as duas linguagens em uma mesma aplicação. Módulos definidos em Erlang são chamados em Elixir através da sintaxe \lstinline|:meu_modulo.funcao()| enquanto o caminha inverso ocorre através da sintaxe \lstinline[language=Erlang]|'Elixir.MeuModulo':funcao()|.

Outra característica interessante das aplicações OTP é o fato de que elas podem ser utilizadas de forma distribuída e com detecção automática de falhas entre as instâncias, de modo que se a instância onde uma aplicação está sendo utilizada cair, outra possa assumir seu lugar de forma totalmente transparente, retornando o controle à primeira instância assim que ela for reiniciada. Um ponto extremamente positivo é que este comportamento requer apenas configurações. Uma aplicação distribuída pode ser executada em apenas uma instância ou em dezenas e se comportar da mesma forma.